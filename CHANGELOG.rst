1.0.0 - Initial version. Added adduser role and adduser+ansible playbooks
1.0.1 - Add args specs for role
1.1.0 - Add ntp role and playbook
1.2.0 - Add hostname role
